import java.time.LocalDate;
import java.util.Scanner;

class TerminaleBanca {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Benvenuto nel sistema bancario!");
        System.out.print("Inserisci il nome del titolare del conto: ");
        String nome = scanner.nextLine();
        System.out.print("Inserisci il cognome del titolare del conto: ");
        String cognome = scanner.nextLine();
        System.out.print("Inserisci il codice fiscale del titolare del conto: ");
        String cf = scanner.nextLine();
        System.out.print("Inserisci il saldo iniziale del conto: ");
        double saldoIniziale = scanner.nextDouble();
        scanner.nextLine(); // Consuma la nuova riga

        ContoCorrente conto = new ContoCorrente(nome, cognome, cf, saldoIniziale);

        while (true) {
            System.out.println("\nOpzioni disponibili:");
            System.out.println("1. Esegui un bonifico");
            System.out.println("2. Esegui un prelievo");
            System.out.println("3. Attiva/Disattiva il fido");
            System.out.println("4. Blocca/Sblocca il conto");
            System.out.println("5. Visualizza saldo");
            System.out.println("6. Esci");

            System.out.print("Scelta: ");
            int scelta = scanner.nextInt();
            scanner.nextLine(); // Consuma la nuova riga

            switch (scelta) {
                case 1:
                    System.out.print("Inserisci la data del bonifico (AAAA-MM-GG): ");
                    LocalDate dataRichiesta = LocalDate.parse(scanner.nextLine());
                    System.out.print("Inserisci la causale del bonifico: ");
                    String causaleBonifico = scanner.nextLine();
                    System.out.print("Inserisci l'importo del bonifico: ");
                    double importoBonifico = scanner.nextDouble();
                    scanner.nextLine(); // Consuma la nuova riga

                    Movimento bonifico = new Movimento(dataRichiesta, LocalDate.now(), causaleBonifico, "Bonifico", importoBonifico);

                    try {
                        conto.bonifico(bonifico);
                        System.out.println("Bonifico effettuato con successo.");
                    } catch (Exception e) {
                        System.out.println("Errore: " + e.getMessage());
                    }
                    break;
                case 2:
                    System.out.print("Inserisci la data del prelievo (AAAA-MM-GG): ");
                    LocalDate dataRichiestaPrelievo = LocalDate.parse(scanner.nextLine());
                    System.out.print("Inserisci la causale del prelievo: ");
                    String causalePrelievo = scanner.nextLine();
                    System.out.print("Inserisci l'importo del prelievo: ");
                    double importoPrelievo = scanner.nextDouble();
                    scanner.nextLine(); // Consuma la nuova riga

                    Movimento prelievo = new Movimento(dataRichiestaPrelievo, LocalDate.now(), causalePrelievo, "Prelievo", importoPrelievo);

                    try {
                        conto.prelievo(prelievo);
                        System.out.println("Prelievo effettuato con successo.");
                    } catch (Exception e) {
                        System.out.println("Errore: " + e.getMessage());
                    }
                    break;
                case 3:
                    if (conto.isBloccato()) {
                        System.out.println("Impossibile attivare/disattivare il fido su un conto bloccato.");
                    } else {
                        System.out.print("Vuoi attivare (1) o disattivare (0) il fido? ");
                        int attivaFido = scanner.nextInt();
                        scanner.nextLine(); // Consuma la nuova riga

                        if (attivaFido == 1) {
                            conto.attivaFido();
                            System.out.println("Fido attivato con successo.");
                        } else if (attivaFido == 0) {
                            conto.disattivaFido();
                            System.out.println("Fido disattivato con successo.");
                        } else {
                            System.out.println("Scelta non valida.");
                        }
                    }
                    break;
                case 4:
                    System.out.print("Vuoi bloccare (1) o sbloccare (0) il conto? ");
                    int bloccaConto = scanner.nextInt();
                    scanner.nextLine(); // Consuma la nuova riga

                    if (bloccaConto == 1) {
                        conto.bloccaConto();
                        System.out.println("Conto bloccato con successo.");
                    } else if (bloccaConto == 0) {
                        conto.sbloccaConto();
                        System.out.println("Conto sbloccato con successo.");
                    } else {
                        System.out.println("Scelta non valida.");
                    }
                    break;
                case 5:
                    System.out.println("Saldo attuale: " + conto.getSaldo());
                    break;
                case 6:
                    System.out.println("Grazie per aver usato il nostro servizio. Arrivederci!");
                    scanner.close();
                    System.exit(0);
                default:
                    System.out.println("Scelta non valida. Riprova.");
            }
        }
    }
}