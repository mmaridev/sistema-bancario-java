public class Persona {
    public final String nome, cognome, cf;

    public Persona(String nome, String cognome, String cf) {
        this.nome = nome;
        this.cognome = cognome;
        this.cf = cf;
    }

    public String toString() {
        return nome + " " + cognome + " (" + cf + ")";
    }
}
